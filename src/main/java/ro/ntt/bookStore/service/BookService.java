package ro.ntt.bookStore.service;

import java.io.File;
import ro.ntt.bookStore.model.Book;

import ro.ntt.bookStore.model.validators.BookValidator;
import ro.ntt.bookStore.model.validators.ValidatorException;
import ro.ntt.bookStore.repository.BookStoreFileRepositry;
import ro.ntt.bookStore.repository.Repository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.DocumentException;
import ro.ntt.bookStore.repository.BookStoreXmlRepositry;
import org.dom4j.Document;
import org.dom4j.Element;

/**
 * Created by radu.
 */
public class BookService {

    private Repository<Long, Book> repository;
    public static int choosenRepositryValue = 0;

    public BookService(Repository<Long, Book> repository) {
        this.repository = repository;
    }

    public void addBook(Book book) throws ValidatorException {
        BookValidator bookValidator = new BookValidator();
        bookValidator.validate(book);
        switch (choosenRepositryValue) {
            case 1:
                repository.save(book);
                break;
            case 2:
                BookStoreFileRepositry bookStoreFileRepositry = new BookStoreFileRepositry(bookValidator, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt");
                bookStoreFileRepositry.saveToFile(book);
                break;
            default:
                BookStoreXmlRepositry bookStoreXmlRepositry = new BookStoreXmlRepositry(bookValidator, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.xml");
                bookStoreXmlRepositry.saveToFile(book);
                break;
        }
    }

    public void deleteBook(Long id) {
        switch (choosenRepositryValue) {
            case 1:
                repository.delete(id);
                break;
            case 2:
                BookStoreFileRepositry bookFileRepo = new BookStoreFileRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt");
                bookFileRepo.deleteFromFile(id);
                break;
            default:
                BookStoreXmlRepositry bookXmlRepo = new BookStoreXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.xml");
                bookXmlRepo.deleteFromFile(id);
                break;
        }

    }

    public void updateBook(Book book) {
        switch (choosenRepositryValue) {
            case 1:
                repository.update(book);
                break;
            case 2:
                BookStoreFileRepositry bookFileRepo = new BookStoreFileRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt");
                bookFileRepo.updateInFile(book);
                break;
            default:
                BookStoreXmlRepositry bookXmlRepo = new BookStoreXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.xml");
                bookXmlRepo.updateInFile(book);
                break;
        }

    }

    public void getAllBooks() {

        switch (choosenRepositryValue) {
            case 1:
                Iterable<Book> books = repository.findAll();
                for (Book b : books) {
                    System.out.println("Id: " + b.getId() + "," + "Name: " + b.getName() + "," + "ISBN Number: " + b.getISBNNumber() + "," + "Published Year: " + b.getYearPublished());
                }
                break;
            case 2:
                Path path = Paths.get("src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt");
                try {
                    Files.lines(path).forEach(System.out::println);//print each line
                } catch (IOException ex) {
                    ex.printStackTrace();//handle exception here
                }
                break;
            default: {

                //  Paths.get("src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt").toUri().toURL();
                BookStoreXmlRepositry bookStoreRepo = new BookStoreXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\bookstore.xml");
                bookStoreRepo.printFromFile();

                break;
            }

        }
    }

    /**
     * Returns all students whose name contain the given string.
     *
     * @param s
     * @return
     */
    public Set<Book> filterBooksByName(String s) {
        Set<Book> filteredBooks = new HashSet<>();
        switch (choosenRepositryValue) {
            case 1:
                Iterable<Book> books = repository.findAll();

                books.forEach(filteredBooks::add);
                filteredBooks.removeIf(book -> !book.getName().contains(s));
                break;
            case 2:
                System.out.println("Filter Books By Name From Text File");
                break;
            default:
                System.out.println("show books from xml file");
                break;
        }

        return filteredBooks;
    }

    /* private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public Set<Book> getAllBooks() {
        Set<Book> books = new HashSet<>();
        bookRepository.findAll().forEach(books::add);
        return books;
    }
     */
}
