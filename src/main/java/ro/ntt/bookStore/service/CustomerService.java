package ro.ntt.bookStore.service;

import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;
import ro.ntt.bookStore.model.validators.BookValidator;
import ro.ntt.bookStore.model.validators.CustomerValidator;
import ro.ntt.bookStore.model.validators.ValidatorException;
import ro.ntt.bookStore.repository.BookStoreFileRepositry;
import ro.ntt.bookStore.repository.CustomerFileRepositry;
import ro.ntt.bookStore.repository.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import ro.ntt.bookStore.repository.BookStoreXmlRepositry;
import ro.ntt.bookStore.repository.CustomerXmlRepositry;

/**
 * Created by radu.
 */
public class CustomerService {

    private Repository<Long, Customer> repository;

    public CustomerService(Repository<Long, Customer> repository) {
        this.repository = repository;
    }

    public void addCustomer(Customer customer) throws ValidatorException {

        CustomerValidator customerValidator = new CustomerValidator();
        customerValidator.validate(customer);

        switch (BookService.choosenRepositryValue) {
            case 1:
                repository.save(customer);
                break;
            case 2:
                CustomerFileRepositry customerFileRepositry = new CustomerFileRepositry(customerValidator, "src\\main\\java\\ro\\ntt\\bookStore\\customer.txt");
                customerFileRepositry.saveToFile(customer);
                break;
            default:
                CustomerXmlRepositry customerXmlRepositry = new CustomerXmlRepositry(customerValidator, "src\\main\\java\\ro\\ntt\\bookStore\\customer.xml");
                customerXmlRepositry.saveToFile(customer);
                break;
        }

    }

    public void updateCustomer(Customer customer) {
        switch (BookService.choosenRepositryValue) {
            case 1:
                repository.update(customer);

                break;
            case 2:
                CustomerFileRepositry customerFileRepo = new CustomerFileRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\customer.txt");
                customerFileRepo.updateInFile(customer);

                break;
            default:
                CustomerXmlRepositry customerXmlRepo = new CustomerXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\customer.xml");
                customerXmlRepo.updateInFile(customer);
                break;
        }

    }

    public void deleteCustomer(Long id) {
        switch (BookService.choosenRepositryValue) {
            case 1:
                repository.delete(id);

                break;
            case 2:
                CustomerFileRepositry customerFileRepo = new CustomerFileRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\customer.txt");
                customerFileRepo.deleteFromFile(id);

                break;
            default:
                CustomerXmlRepositry customerXmlRepo = new CustomerXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\customer.xml");
                customerXmlRepo.deleteFromFile(id);
                break;
        }

    }

    public void getAllCustomers() {

        switch (BookService.choosenRepositryValue) {
            case 1:
                Iterable<Customer> customers = repository.findAll();
                for (Customer c : customers) {
                    System.out.println("Customer ID: " + c.getId() + "," + "Customer Name: " + c.getName() + "," + "Customer CNP:" + c.getCNP());
                }
                break;
            case 2:
                Path path = Paths.get("src\\main\\java\\ro\\ntt\\bookStore\\customer.txt");
                try {
                    Files.lines(path).forEach(System.out::println);//print each line
                } catch (IOException ex) {
                    ex.printStackTrace();//handle exception here
                }
                break;
            default:
                CustomerXmlRepositry customerXmlRepo = new CustomerXmlRepositry(null, "src\\main\\java\\ro\\ntt\\bookStore\\customer.xml");
                customerXmlRepo.printFromFile();
                break;
        }

    }

    /**
     * Returns all students whose name contain the given string.
     *
     * @param s
     * @return
     */
}
