package ro.ntt.bookStore.ui;

import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;
import ro.ntt.bookStore.model.validators.ValidatorException;
import ro.ntt.bookStore.service.BookService;
import ro.ntt.bookStore.service.CustomerService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by radu.
 */
public class Console {

    private BookService bookService;
    private CustomerService customerService;

    public Console(BookService bookService, CustomerService customerService) {
        this.customerService = customerService;
        this.bookService = bookService;
    }

    public void runConsole() {
        addBooks();
        printAllBooks();
        filterBooks();
    }

    private void filterBooks() {
        System.out.println("filtered books (name containing 'LALA'):");
        Set<Book> books = bookService.filterBooksByName("LALA");
        books.stream().forEach(System.out::println);
    }

    private void addBooks() {
        while (true) {
            Book book = readBook();
            if (book == null || book.getId() < 0) {
                break;
            }
            try {
                bookService.addBook(book);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
        }
    }

    private Book readBook() {
        System.out.println("Read book {id,serialNumber, title, publishesYear}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.valueOf(bufferRead.readLine());// ...
            String serialNumber = bufferRead.readLine();
            String name = bufferRead.readLine();
            int publishedYear = Integer.parseInt(bufferRead.readLine());// ...

            Book book = new Book(id, serialNumber, name, publishedYear);
            book.setId(id);

            return book;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /*private BookService bookService;

    public Console(BookService bookService) {
        this.bookService = bookService;
    }
     */
    private void chooseRepositry() {
        System.out.println("Choose your selected repositry\n"
                + "1 - In Memory Repositry\n"
                + "2 - File Repositry\n"
                + "3 - XML Repositry\n"
                + "x - Exit");

        Scanner repositryInput = new Scanner(System.in);
        String repositryOption = repositryInput.next();
        if (repositryOption.equals("x")) {
            System.out.print("Bye");
            System.exit(0);
        }
        switch (repositryOption) {
            case "1":

                BookService.choosenRepositryValue = 1;
                break;
            case "2":
                BookService.choosenRepositryValue = 2;
                break;
            case "3":
                BookService.choosenRepositryValue = 3;
                break;
        }
    }

    public void runMenu() {

        chooseRepositry();

        printMenu();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String option = scanner.next();
            if (option.equals("x")) {
                break;
            }
            switch (option) {
                case "1":
                    addBook();
                    break;
                case "2":
                    printAllBooks();
                    break;

                case "3":
                    delete();
                    break;

                case "4":
                    updateBook();
                    break;

                case "5":
                    addCustomer();
                    break;

                case "6":
                    printAllCustomers();
                    break;
                case "7":
                    deleteCustomer();
                    break;
                case "8":
                    updateCustomer();
                    break;

                default:
                    System.out.println("this option is not yet implemented");
            }
            printMenu();
        }
    }

    private void updateBook() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();

        System.out.println("New ISBN number = ");
        String serialNumber = scanner.next();

        System.out.println("New title = ");
        String name = scanner.next();

        System.out.println("New year of publishing = ");
        int groupNumber = scanner.nextInt();

        Book book = new Book(id, serialNumber, name, groupNumber);
        bookService.updateBook(book);
    }

    private void updateCustomer() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();

        System.out.println("name = ");
        String name = scanner.next();

        System.out.println("CNP  = ");
        String cnp = scanner.next();

        Customer customer = new Customer(id, name, cnp);
        customerService.updateCustomer(customer);
    }

    private void printAllBooks() {
        System.out.println("All books: \n");
        bookService.getAllBooks();
    }

    private void addBook() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();

        System.out.println("ISBN number = ");
        String serialNumber = scanner.next();

        System.out.println("title = ");
        String name = scanner.next();

        System.out.println("year of publishing = ");
        int groupNumber = scanner.nextInt();

        Book book = new Book(id, serialNumber, name, groupNumber);
        bookService.addBook(book);
    }

    private void printMenu() {
        System.out.println("1 - Add Book\n"
                + "2 - Print all books\n"
                + "3 - Delete after id\n"
                + "4 - Update Book After Id\n"
                + "5 - Add Customer\n"
                + "6 - Print all Customers\n"
                + "7 - Delete Customer after id\n"
                + "8 - Update Customer After id\n"
                + "x - Exit");
    }

    public void delete() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();
        bookService.deleteBook(id);

    }

    public void runCustomerConsole() {
        addCustomers();
        printAllCustomers();
    }

    private void addCustomers() {
        while (true) {
            Customer customer = readCustomer();
            if (customer == null || customer.getId() < 0) {
                break;
            }
            try {
                customerService.addCustomer(customer);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
        }
    }

    private Customer readCustomer() {
        System.out.println("Read Customer {id,name, cnp}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.valueOf(bufferRead.readLine());// ...
            String name = bufferRead.readLine();
            String cnp = bufferRead.readLine();

            Customer customer = new Customer(id, name, cnp);
            customer.setId(id);

            return customer;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /*private BookService bookService;

    public Console(BookService bookService) {
        this.bookService = bookService;
    }
     */
    private void printAllCustomers() {
        System.out.println("All Customers: \n");
        customerService.getAllCustomers();
    }

    private void addCustomer() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();

        System.out.println("name = ");
        String name = scanner.next();

        System.out.println("CNP  = ");
        String cnp = scanner.next();

        Customer customer = new Customer(id, name, cnp);
        customerService.addCustomer(customer);
    }

    public void deleteCustomer() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("id = ");
        Long id = scanner.nextLong();
        customerService.deleteCustomer(id);

    }
}
