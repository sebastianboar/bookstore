/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.ntt.bookStore.repository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.w3c.dom.Text;
//import ro.ntt.bookStore.model.BaseEntity;
import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.validators.Validator;
import ro.ntt.bookStore.model.validators.ValidatorException;

///////////////
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ro.ntt.bookStore.service.BookService;


public class BookStoreXmlRepositry extends InMemoryRepository<Long, Book> {

    private String fileName;

    public BookStoreXmlRepositry(Validator<Book> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        //  loadData();
    }

    public static Document parse(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String serialNumber = items.get(1);
                String name = items.get((2));
                int yearPublished = Integer.parseInt(items.get(3));

                Book book = new Book(id, serialNumber, name, yearPublished);
                book.setId(id);

                try {
                    super.save(book);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException {
        Optional<Book> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }

        saveToFile(entity);

        return Optional.empty();
    }

    List<Book> readBookFromXMLFile(String filePath) {
        List<Book> listBooks = new ArrayList<Book>();
        try {
            //  Paths.get("src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt").toUri().toURL();

            File myFile = new File(filePath);
            URL myUrl = myFile.toURI().toURL();
            Document doc = BookStoreXmlRepositry.parse(myUrl);
            Element root = doc.getRootElement();
            //Iterate now
            for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
                Element element = it.next();
                String ISBNNumber = "";
                String name = "";
                int yearPublished = -1;
                long id = 0;

                for (Iterator<Element> iter = element.elementIterator(); iter.hasNext();) {
                    Element elem = iter.next();
                    if (elem.getName().equals("id")) {
                        id = Long.parseLong(elem.getData().toString().trim());
                    } else if (elem.getName().equals("Name")) {
                        name = elem.getData().toString().trim();
                    } else if (elem.getName().equals("ISBNNumber")) {
                        ISBNNumber = elem.getData().toString().trim();
                    } else if (elem.getName().equals("YearOfPublish")) {
                        yearPublished = Integer.parseInt(elem.getData().toString().trim());
                    }

                    // System.out.print(" " + elem.getName() + " , " + elem.getData().toString());
                }
                Book book = new Book(id, name, ISBNNumber, yearPublished);
                listBooks.add(book);
            }
            //  System.out.print("\n");
        } catch (MalformedURLException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBooks;
    }

    public void saveToFile(Book entity) {

        Path path = Paths.get(fileName);

        File f = new File(fileName);
        if (!f.exists()) {

            FileOutputStream fos = null;

            try {

                //Write here
                Document document = DocumentFactory.getInstance().createDocument();
                // Create the root element of xml file
                Element root = document.addElement("BookStore");

                Element book = root.addElement("book");

                // Add the element name in root element.
                Element id = book.addElement("id");
                id.addText(entity.getId().toString());
                // Add the element age in root element.
                Element name = book.addElement("Name");
                name.addText(entity.getName());

                Element isbnNumber = book.addElement("ISBNNumber");
                isbnNumber.addText(entity.getISBNNumber());

                Element yearOfPublish = book.addElement("YearOfPublish");
                yearOfPublish.addText(Integer.toString(entity.getYearPublished()));
                // Create a file named as person.xml
                fos = new FileOutputStream(fileName);
                // Create the pretty print of xml document.
                OutputFormat format = OutputFormat.createCompactFormat();
                // Create the xml writer by passing outputstream and format
                XMLWriter writer = new XMLWriter(fos, format);
                // Write to the xml document
                writer.write(document);
                // Flush after done
                writer.flush();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            try {
                List<Book> alreadyAvailableBooks = readBookFromXMLFile(fileName);

                FileOutputStream fos = null;

                Document document = DocumentFactory.getInstance().createDocument();
                Element root = document.addElement("BookStore");

                for (int i = 0; i < alreadyAvailableBooks.size(); i++) {
                    Element book = root.addElement("book");
                    // Add the element name in root element.
                    Element id = book.addElement("id");
                    id.addText(alreadyAvailableBooks.get(i).getId().toString());
                    // Add the element name in root element.
                    Element name = book.addElement("Name");
                    name.addText(alreadyAvailableBooks.get(i).getName().toString());

                    Element isbnNumber = book.addElement("ISBNNumber");
                    isbnNumber.addText(alreadyAvailableBooks.get(i).getISBNNumber().toString());
                    // Add the element name in root element.
                    Element yearOfPublish = book.addElement("YearOfPublish");
                    yearOfPublish.addText(String.valueOf(alreadyAvailableBooks.get(i).getYearPublished()));

                }

                Element book = root.addElement("book");
                // Add the element name in root element.
                Element id = book.addElement("id");
                id.addText(entity.getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(entity.getName());

                Element isbnNumber = book.addElement("ISBNNumber");
                isbnNumber.addText(entity.getISBNNumber());
                // Add the element name in root element.
                Element yearOfPublish = book.addElement("YearOfPublish");
                yearOfPublish.addText(String.valueOf(entity.getYearPublished()));

                // Create a file named as person.xml
                fos = new FileOutputStream(fileName);
                // Create the pretty print of xml document.
                OutputFormat format = OutputFormat.createCompactFormat();
                // Create the xml writer by passing outputstream and format
                XMLWriter writer = new XMLWriter(fos, format);
                // Write to the xml document
                writer.write(document);
                // Flush after done
                writer.flush();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void deleteFromFile(Long id) {

        try {
            FileOutputStream fos = null;
            List<Book> alreadyAvailableBooks = readBookFromXMLFile(fileName);

            Document document = DocumentFactory.getInstance().createDocument();
            Element root = document.addElement("BookStore");

            for (int i = 0; i < alreadyAvailableBooks.size(); i++) {

                if (alreadyAvailableBooks.get(i).getId().equals(id)) {
                    alreadyAvailableBooks.remove(i);
                    i--;
                }

            }

            for (int i = 0; i < alreadyAvailableBooks.size(); i++) {
                Element book = root.addElement("book");
                // Add the element name in root element.
                Element idElement = book.addElement("id");
                idElement.addText(alreadyAvailableBooks.get(i).getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(alreadyAvailableBooks.get(i).getName().toString());

                Element isbnNumber = book.addElement("ISBNNumber");
                isbnNumber.addText(alreadyAvailableBooks.get(i).getISBNNumber().toString());
                // Add the element name in root element.
                Element yearOfPublish = book.addElement("YearOfPublish");
                yearOfPublish.addText(String.valueOf(alreadyAvailableBooks.get(i).getYearPublished()));

            }

            // Create a file named as person.xml
            fos = new FileOutputStream(fileName);
            // Create the pretty print of xml document.
            OutputFormat format = OutputFormat.createCompactFormat();
            // Create the xml writer by passing outputstream and format
            XMLWriter writer = new XMLWriter(fos, format);
            // Write to the xml document
            writer.write(document);
            // Flush after done
            writer.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateInFile(Book entity) {

        try {
            FileOutputStream fos = null;
            List<Book> alreadyAvailableBooks = readBookFromXMLFile(fileName);

            Document document = DocumentFactory.getInstance().createDocument();
            Element root = document.addElement("BookStore");

            for (int i = 0; i < alreadyAvailableBooks.size(); i++) {

                if (alreadyAvailableBooks.get(i).getId().equals(entity.getId())) {
                    alreadyAvailableBooks.remove(i);
                    alreadyAvailableBooks.add(entity);
                    //i--;
                }

            }

            for (int i = 0; i < alreadyAvailableBooks.size(); i++) {
                Element book = root.addElement("book");
                // Add the element name in root element.
                Element idElement = book.addElement("id");
                idElement.addText(alreadyAvailableBooks.get(i).getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(alreadyAvailableBooks.get(i).getName().toString());

                Element isbnNumber = book.addElement("ISBNNumber");
                isbnNumber.addText(alreadyAvailableBooks.get(i).getISBNNumber().toString());
                // Add the element name in root element.
                Element yearOfPublish = book.addElement("YearOfPublish");
                yearOfPublish.addText(String.valueOf(alreadyAvailableBooks.get(i).getYearPublished()));

            }

            // Create a file named as person.xml
            fos = new FileOutputStream(fileName);
            // Create the pretty print of xml document.
            OutputFormat format = OutputFormat.createCompactFormat();
            // Create the xml writer by passing outputstream and format
            XMLWriter writer = new XMLWriter(fos, format);
            // Write to the xml document
            writer.write(document);
            // Flush after done
            writer.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printFromFile() {
        List<Book> allBooks = readBookFromXMLFile(fileName);

        for (Book book : allBooks) {

            System.out.println("Id: " + book.getId() + "," + "Name: " + book.getName() + "," + "ISB Number:" + book.getISBNNumber() + "," + "Published Year:" + book.getYearPublished());

        }

    }

}
