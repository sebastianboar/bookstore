package ro.ntt.bookStore.repository;

import java.io.BufferedReader;
import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;
import ro.ntt.bookStore.model.validators.Validator;
import ro.ntt.bookStore.model.validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomerFileRepositry extends InMemoryRepository<Long, Customer> {

    private String fileName;

    public CustomerFileRepositry(Validator<Customer> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        //  loadData();
    }

    public void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String name = items.get(1);
                String cnp = items.get(2);

                Customer customer = new Customer(id, name, cnp);
                customer.setId(id);

                try {
                    super.save(customer);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Customer> save(Customer entity) throws ValidatorException {
        Optional<Customer> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    public void saveToFile(Customer entity) {
        Path path = Paths.get(fileName);
        File f = new File(fileName);
        if (!f.exists()) {
            try {
                //create that file
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(BookStoreFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getName() + "," + entity.getCNP());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFromFile(Long id) {
        Path path = Paths.get(fileName);
        List<Customer> availableCustomers = readFromTextFile();
        System.out.println(availableCustomers.size());

        for (int i = 0; i < availableCustomers.size(); i++) {

            if (availableCustomers.get(i).getId().equals(id)) {
                availableCustomers.remove(i);
                i--;
            }

        }
        //Remove File Contents
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.TRUNCATE_EXISTING)) {
            bufferedWriter.write("");
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Now Write it again to the specified file
        for (Customer c : availableCustomers) {

            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
                bufferedWriter.write(
                        c.getId() + "," + c.getName() + "," + c.getCNP());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    List<Customer> readFromTextFile() {

        BufferedReader br = null;

        List<Customer> customers = new ArrayList<Customer>();
        try {
            File file = new File(fileName);
            br = new BufferedReader(new FileReader(file));
            String st;
            String cnp = "";
            String name = "";
            long id = 0;
            while ((st = br.readLine()) != null) {
                if ((st.trim()).length() > 0) {
                    List<String> elementsList = Arrays.asList(st.split(","));

                    id = Long.parseLong(elementsList.get(0));
                    name = elementsList.get(1);
                    cnp = elementsList.get(2);
                    Customer customer = new Customer(id, name, cnp);
                    customers.add(customer);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return customers;
    }

    public void updateInFile(Customer customer) {
        Path path = Paths.get(fileName);
        List<Customer> availableCustomers = readFromTextFile();
        System.out.println(availableCustomers.size());

        for (int i = 0; i < availableCustomers.size(); i++) {

            if (availableCustomers.get(i).getId().equals(customer.getId())) {
                availableCustomers.remove(i);
                availableCustomers.add(customer);
                //i--;
            }

        }
        //Remove File Contents
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.TRUNCATE_EXISTING)) {
            bufferedWriter.write("");
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Now Write it again to the specified file
        for (Customer c : availableCustomers) {

            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
                bufferedWriter.write(
                        c.getId() + "," + c.getName() + "," + c.getCNP());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
