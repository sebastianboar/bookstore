package ro.ntt.bookStore.repository;

import ro.ntt.bookStore.model.Book;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */
public class BookRepositoryImpl implements BookRepository {
    private Map<Long, Book> entities;


    public BookRepositoryImpl() {
        this.entities = new HashMap<>();
    }

    @Override
    public Book findOne(Long id) {
        throw new RuntimeException("not yet implemented");
    }


    /*
     * finds all entities of type book.

     * @returns all books found.
     */
    @Override
    public Iterable<Book> findAll() {
        Set<Book> books = entities.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toSet());
        return books;
    }
    /*
     * Saves the given book object entity.
     *
     * @param entity must be the object of Book.
     * @throws IllegaalArgumentException if the entity is null.
     */
    @Override
    public Book save(Book entity) {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        return entities.putIfAbsent(entity.getId(), entity);
    }

    /*
     * Delete the book with the given id.
     *
     * @param id must be found among the the books objects.
     * @returns the book deleted
     * @throws IllegalArgumentException if the given id is not found.
     */
    @Override
    public Book delete(Long id) {
        Book b = entities.get(id);
        if (b == null) {
            throw new IllegalArgumentException("id cannot be associated with a book");
        }
        entities.remove(id);
        return b;

    }

   public Book update(Book entity) {throw new RuntimeException("not yet implemented");}
}
