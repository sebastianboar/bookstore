package ro.ntt.bookStore.repository;
import ro.ntt.bookStore.model.Customer;

    public interface CustomerRepository {
        /**
         * Find the entity with the given {@code id}.
         *
         * @param id must be not null.
         * @return a {@code Book}  with the given id.
         * @throws IllegalArgumentException if the given id is null.
         */
        Customer findOne(Long id);

        /**
         * @return all entities.
         */
        Iterable<Customer> findAll();

        /**
         * Saves the given entity.
         *
         * @param entity must not be null.
         * @return a {@code Book} - null if the entity was saved otherwise (e.g. id already exists) returns the entity.
         * @throws IllegalArgumentException if the given entity is null.
         */
        Customer save(Customer entity);

        /**
         * Removes the entity with the given id.
         *
         * @param id must not be null.
         * @return a {@code Book} - null if there is no entity with the given id, otherwise the removed entity.
         * @throws IllegalArgumentException if the given id is null.
         */
        Customer delete(Long id);

        /**
         * Updates the given entity.
         *
         * @param entity must not be null.
         * @return a {@code Book} - null if the entity was updated otherwise (e.g. id does not exist) returns the
         * entity.
         * @throws IllegalArgumentException if the given entity is null.
         */
        Customer update(Customer entity);
    }

