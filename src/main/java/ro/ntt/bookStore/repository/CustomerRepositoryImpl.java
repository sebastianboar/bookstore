package ro.ntt.bookStore.repository;

import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CustomerRepositoryImpl implements CustomerRepository  {
    private Map<Long, Customer> entities;

    public CustomerRepositoryImpl() {
        this.entities = new HashMap<>();
    }
    @Override
    public Customer findOne(Long id) {
        return null;
    }

    @Override
    public Iterable<Customer> findAll() {
        Set<Customer> customers = entities.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toSet());
        return customers;
    }

    @Override
    public Customer save(Customer entity) {
        System.out.println(entity.getId()+" "+entity.getName()+" "+entity.getCNP());
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        return entities.putIfAbsent(entity.getId(), entity);
    }
    @Override
    public Customer delete(Long id) {
        Customer c=entities.get(id);
        if (c == null) {
                throw new IllegalArgumentException("id cannot be associated with a customer");
            }
            entities.remove(id);
            return c;
    }

    @Override
    public Customer update(Customer entity) {
        return null;
    }
}
