package ro.ntt.bookStore.repository;

import ro.ntt.bookStore.model.Book;

/**
 * Interface for CRUD operations on a Book.
 *
 * @author radu.
 */
public interface BookRepository {
    /**
     * Find the entity with the given {@code id}.
     *
     * @param id must be not null.
     * @return a {@code Book}  with the given id.
     * @throws IllegalArgumentException if the given id is null.
     */
    Book findOne(Long id);

    /**
     * @return all entities.
     */
    Iterable<Book> findAll();

    /**
     * Saves the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Book} - null if the entity was saved otherwise (e.g. id already exists) returns the entity.
     * @throws IllegalArgumentException if the given entity is null.
     */
    Book save(Book entity);

    /**
     * Removes the entity with the given id.
     *
     * @param id must not be null.
     * @return a {@code Book} - null if there is no entity with the given id, otherwise the removed entity.
     * @throws IllegalArgumentException if the given id is null.
     */
    Book delete(Long id);

    /**
     * Updates the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Book} - null if the entity was updated otherwise (e.g. id does not exist) returns the
     * entity.
     * @throws IllegalArgumentException if the given entity is null.
     */
    Book update(Book entity);
}
