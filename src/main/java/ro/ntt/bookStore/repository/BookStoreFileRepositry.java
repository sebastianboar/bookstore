package ro.ntt.bookStore.repository;

import java.io.BufferedReader;
import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.validators.Validator;
import ro.ntt.bookStore.model.validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import ro.ntt.bookStore.model.Customer;

public class BookStoreFileRepositry extends InMemoryRepository<Long, Book> {

    private String fileName;

    public BookStoreFileRepositry(Validator<Book> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        //  loadData();
    }

    public void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String serialNumber = items.get(1);
                String name = items.get((2));
                int yearPublished = Integer.parseInt(items.get(3));

                Book book = new Book(id, serialNumber, name, yearPublished);
                book.setId(id);

                try {
                    super.save(book);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException {
        Optional<Book> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    public void saveToFile(Book entity) {
        Path path = Paths.get(fileName);
        File f = new File(fileName);
        if (!f.exists()) {
            try {
                //create that file
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(BookStoreFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getISBNNumber() + "," + entity.getName() + "," + entity.getYearPublished());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List<Book> readFromTextFile() {

        BufferedReader br = null;

        List<Book> books = new ArrayList<Book>();
        try {
            File file = new File(fileName);
            br = new BufferedReader(new FileReader(file));
            String st;
            int yearOfPublish = 0;
            String isbnNumber = "";
            String name = "";
            long id = 0;
            while ((st = br.readLine()) != null) {
                if ((st.trim()).length() > 0) {
                    List<String> elementsList = Arrays.asList(st.split(","));

                    id = Long.parseLong(elementsList.get(0));
                    name = elementsList.get(1);
                    isbnNumber = elementsList.get(2);
                    yearOfPublish = Integer.parseInt(elementsList.get(3));
                    Book book = new Book(id, isbnNumber, name, yearOfPublish);
                    books.add(book);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(CustomerFileRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return books;
    }

    public void deleteFromFile(long id) {

        Path path = Paths.get(fileName);
        List<Book> availableBooks = readFromTextFile();

        for (int i = 0; i < availableBooks.size(); i++) {

            if (availableBooks.get(i).getId().equals(id)) {
                availableBooks.remove(i);
                i--;
            }

        }

        //remove previous data
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.TRUNCATE_EXISTING)) {
            bufferedWriter.write("");
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Now Write it again to the specified file
        for (Book b : availableBooks) {

            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
                bufferedWriter.write(
                        b.getId() + "," + b.getName() + "," + b.getISBNNumber() + "," + b.getYearPublished());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public void updateInFile(Book entity) {

        Path path = Paths.get(fileName);
        List<Book> availableBooks = readFromTextFile();

        for (int i = 0; i < availableBooks.size(); i++) {

            if (availableBooks.get(i).getId().equals(entity.getId())) {
                availableBooks.remove(i);
                availableBooks.add(entity);
                //   i--;
            }

        }

        //remove previous data
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.TRUNCATE_EXISTING)) {
            bufferedWriter.write("");
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Now Write it again to the specified file
        for (Book b : availableBooks) {

            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
                bufferedWriter.write(
                        b.getId() + "," + b.getName() + "," + b.getISBNNumber() + "," + b.getYearPublished());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
