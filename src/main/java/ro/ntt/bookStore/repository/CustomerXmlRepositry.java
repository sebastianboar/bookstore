/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.ntt.bookStore.repository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;
import ro.ntt.bookStore.model.validators.Validator;
import ro.ntt.bookStore.model.validators.ValidatorException;
import ro.ntt.bookStore.service.BookService;


public class CustomerXmlRepositry extends InMemoryRepository<Long, Customer> {

    private String fileName;

    public CustomerXmlRepositry(Validator<Customer> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        // loadData();
    }

    public void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String name = items.get(1);
                String cnp = items.get(2);

                Customer customer = new Customer(id, name, cnp);
                customer.setId(id);

                try {
                    super.save(customer);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Customer> save(Customer entity) throws ValidatorException {
        Optional<Customer> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    public void saveToFile(Customer entity) {

        File f = new File(fileName);
        if (!f.exists()) {

            FileOutputStream fos = null;

            try {

                //Write here
                Document document = DocumentFactory.getInstance().createDocument();
                // Create the root element of xml file
                Element root = document.addElement("CustomerStore");

                Element book = root.addElement("customer");

                // Add the element name in root element.
                Element id = book.addElement("id");
                id.addText(entity.getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(entity.getName());

                Element isbnNumber = book.addElement("cnp");
                isbnNumber.addText(entity.getCNP());

                // Create a file named as customer.xml
                fos = new FileOutputStream(fileName);
                // Create the pretty print of xml document.
                OutputFormat format = OutputFormat.createCompactFormat();
                // Create the xml writer by passing outputstream and format
                XMLWriter writer = new XMLWriter(fos, format);
                // Write to the xml document
                writer.write(document);
                // Flush after done
                writer.flush();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            try {
                List<Customer> alreadyPresentCustomers = readCustomerFromXMLFile(fileName);

                FileOutputStream fos = null;

                Document document = DocumentFactory.getInstance().createDocument();
                Element root = document.addElement("CustomerStore");

                for (int i = 0; i < alreadyPresentCustomers.size(); i++) {
                    Element book = root.addElement("customer");
                    // Add the element name in root element.
                    Element id = book.addElement("id");
                    id.addText(alreadyPresentCustomers.get(i).getId().toString());
                    // Add the element name in root element.
                    Element name = book.addElement("Name");
                    name.addText(alreadyPresentCustomers.get(i).getName().toString());

                    Element isbnNumber = book.addElement("cnp");
                    isbnNumber.addText(alreadyPresentCustomers.get(i).getCNP().toString());

                }

                Element book = root.addElement("customer");
                // Add the element name in root element.
                Element id = book.addElement("id");
                id.addText(entity.getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(entity.getName());

                Element cnpNumber = book.addElement("cnp");
                cnpNumber.addText(entity.getCNP());
                // Add the element name in root element.

                // Create a file named as person.xml
                fos = new FileOutputStream(fileName);
                // Create the pretty print of xml document.
                OutputFormat format = OutputFormat.createCompactFormat();
                // Create the xml writer by passing outputstream and format
                XMLWriter writer = new XMLWriter(fos, format);
                // Write to the xml document
                writer.write(document);
                // Flush after done
                writer.flush();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    List<Customer> readCustomerFromXMLFile(String filePath) {
        List<Customer> listCustomers = new ArrayList<Customer>();
        try {
            //  Paths.get("src\\main\\java\\ro\\ntt\\bookStore\\bookstore.txt").toUri().toURL();

            File myFile = new File(filePath);
            URL myUrl = myFile.toURI().toURL();
            Document doc = BookStoreXmlRepositry.parse(myUrl);
            Element root = doc.getRootElement();
            //Iterate now
            for (Iterator<org.dom4j.Element> it = root.elementIterator(); it.hasNext();) {
                Element element = it.next();
                String cnp = "";
                String name = "";
                long id = 0;

                for (Iterator<org.dom4j.Element> iter = element.elementIterator(); iter.hasNext();) {
                    org.dom4j.Element elem = iter.next();
                    if (elem.getName().equals("id")) {
                        id = Long.parseLong(elem.getData().toString().trim());
                    } else if (elem.getName().equals("Name")) {
                        name = elem.getData().toString().trim();
                    } else if (elem.getName().equals("cnp")) {
                        cnp = elem.getData().toString().trim();
                    }

                    // System.out.print(" " + elem.getName() + " , " + elem.getData().toString());
                }
                Customer customer = new Customer(id, name, cnp);
                listCustomers.add(customer);
            }
            //  System.out.print("\n");
        } catch (MalformedURLException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCustomers;
    }

    public void deleteFromFile(Long id) {

        try {
            FileOutputStream fos = null;
            List<Customer> alreadyAvailableCustomers = readCustomerFromXMLFile(fileName);

            Document document = DocumentFactory.getInstance().createDocument();
            Element root = document.addElement("CustomerStore");

            for (int i = 0; i < alreadyAvailableCustomers.size(); i++) {

                if (alreadyAvailableCustomers.get(i).getId().equals(id)) {
                    alreadyAvailableCustomers.remove(i);
                    i--;
                }

            }

            for (int i = 0; i < alreadyAvailableCustomers.size(); i++) {
                Element book = root.addElement("customer");
                // Add the element name in root element.
                Element idElement = book.addElement("id");
                idElement.addText(alreadyAvailableCustomers.get(i).getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(alreadyAvailableCustomers.get(i).getName().toString());

                Element isbnNumber = book.addElement("cnp");
                isbnNumber.addText(alreadyAvailableCustomers.get(i).getCNP().toString());

            }

            // Create a file named as person.xml
            fos = new FileOutputStream(fileName);
            // Create the pretty print of xml document.
            OutputFormat format = OutputFormat.createCompactFormat();
            // Create the xml writer by passing outputstream and format
            XMLWriter writer = new XMLWriter(fos, format);
            // Write to the xml document
            writer.write(document);
            // Flush after done
            writer.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateInFile(Customer customer) {

        try {
            FileOutputStream fos = null;
            List<Customer> alreadyAvailableCustomers = readCustomerFromXMLFile(fileName);

            Document document = DocumentFactory.getInstance().createDocument();
            Element root = document.addElement("CustomerStore");

            for (int i = 0; i < alreadyAvailableCustomers.size(); i++) {

                if (alreadyAvailableCustomers.get(i).getId().equals(customer.getId())) {
                    alreadyAvailableCustomers.remove(i);
                    alreadyAvailableCustomers.add(customer);
                    //i--;
                }

            }

            for (int i = 0; i < alreadyAvailableCustomers.size(); i++) {
                Element book = root.addElement("customer");
                // Add the element name in root element.
                Element idElement = book.addElement("id");
                idElement.addText(alreadyAvailableCustomers.get(i).getId().toString());
                // Add the element name in root element.
                Element name = book.addElement("Name");
                name.addText(alreadyAvailableCustomers.get(i).getName().toString());

                Element isbnNumber = book.addElement("cnp");
                isbnNumber.addText(alreadyAvailableCustomers.get(i).getCNP().toString());

            }

            // Create a file named as person.xml
            fos = new FileOutputStream(fileName);
            // Create the pretty print of xml document.
            OutputFormat format = OutputFormat.createCompactFormat();
            // Create the xml writer by passing outputstream and format
            XMLWriter writer = new XMLWriter(fos, format);
            // Write to the xml document
            writer.write(document);
            // Flush after done
            writer.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BookStoreXmlRepositry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printFromFile() {
        List<Customer> allCustomers = readCustomerFromXMLFile(fileName);

        for (Customer customer : allCustomers) {

            System.out.println("Customer Id: " + customer.getId() + "," + "Customer Name:" + customer.getName() + "," + "Customer CNP:" + customer.getCNP());

        }

    }

}
