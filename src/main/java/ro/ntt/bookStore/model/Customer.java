package ro.ntt.bookStore.model;

public class Customer extends BaseEntity<Long> {

    private String name;
    private String CNP;


    public Customer(Long id, String name, String CNP){
        super(id);
        this.name = name;
        this.CNP = CNP;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getCNP() { return CNP; }

    public void setCNP(String CNP) { this.CNP = CNP; }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (!CNP.equals(customer.CNP)) return false;
        return name.equals(customer.name);

    }

    @Override
    public int hashCode() {
        int result = CNP.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "CNP='" + CNP + '\'' +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }

}
