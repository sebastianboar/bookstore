package ro.ntt.bookStore.model;

/**
 * @author radu.
 */
public class Book extends BaseEntity<Long> {
    private String ISBNNumber;
    private String name;
    private int yearPublished;


    public Book(Long id, String ISBNNumber, String name, int group) {
        super(id);
        this.ISBNNumber = ISBNNumber;
        this.name = name;
        this.yearPublished = group;
    }
    

    public String getISBNNumber() {
        return ISBNNumber;
    }

    public void setISBNNumber(String ISBNNumber) {
        this.ISBNNumber = ISBNNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(int yearPublished) {
        this.yearPublished = yearPublished;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (yearPublished != book.yearPublished) return false;
        if (!ISBNNumber.equals(book.ISBNNumber)) return false;
        return name.equals(book.name);

    }

    @Override
    public int hashCode() {
        int result = ISBNNumber.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + yearPublished;
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "ISBNNumber='" + ISBNNumber + '\'' +
                ", name='" + name + '\'' +
                ", yearPublished=" + yearPublished +
                "} " + super.toString();
    }
}
