package ro.ntt.bookStore.model.validators;

public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
