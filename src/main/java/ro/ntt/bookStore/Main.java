package ro.ntt.bookStore;

import ro.ntt.bookStore.model.Book;
import ro.ntt.bookStore.model.Customer;
import ro.ntt.bookStore.model.validators.BookValidator;
import ro.ntt.bookStore.model.validators.CustomerValidator;
import ro.ntt.bookStore.model.validators.Validator;
import ro.ntt.bookStore.repository.InMemoryRepository;
import ro.ntt.bookStore.repository.Repository;
import ro.ntt.bookStore.service.BookService;
import ro.ntt.bookStore.service.CustomerService;
import ro.ntt.bookStore.ui.Console;

/**
 * Created by radu.
 */
public class Main {

    public static void main(String[] args) {

        Validator<Book> bookValidator = new BookValidator();
        Repository<Long, Book> bookRepository = new InMemoryRepository<>(bookValidator);
        BookService bookService = new BookService(bookRepository);

        Validator<Customer> customerValidator = new CustomerValidator();
        Repository<Long, Customer> customerRepository = new InMemoryRepository<>(customerValidator);
        CustomerService customerService = new CustomerService(customerRepository);
        Console console = new Console(bookService, customerService);
        console.runMenu();

        /* BookRepository bookRepository =new BookRepositoryImpl();3
        1
        
        BookService bookService =new BookService(bookRepository);
        Console console=new Console(bookService);

        Book book1 = new Book(new Long(1), "VRERE43", "LALA",1992);
        Book book2 = new Book(new Long(2), "VREG54", "LALU",1993);
        Book book3 = new Book(new Long(3), "VREG54", "LAUA",1995);
        bookService.addBook(book1);
        bookService.addBook(book2);
        bookService.addBook(book3);

         */
        System.out.println("bye");
    }
}
